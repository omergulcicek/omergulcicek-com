<p align="center">
<img src="https://omergulcicek.com/img/omergulcicek-v3.png" alt="Ömer Gülçiçek" height="100">
</p>

<a href="https://omergulcicek.com/" target="_blank"><h1 align="center">omergulcicek.com</h1></a>

<a href="https://turkuazcss.com/" target="_blank">Turkuaz Css</a> kullanılarak geliştirilmiştir.

---

### Tasarım

Tasarım olarak <a href="https://creativegigs.net/">creativegigs</a>'in bir tasarımı baz alarak sıfırdan geliştirdim.

### Yazı Stili

Yazı stili olarak <a href="https://fonts.google.com/specimen/Poppins">Poppins</a> kullanılmıştır.

---

<i>Takıldığınız yerde yada anlamadığınız kodda iletişime geçebilirsiniz.</i>
